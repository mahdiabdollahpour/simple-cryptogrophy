import java.util.ArrayList;

/**
 * an abstract class just haaving the prototype of to methods
 */

public abstract class BaseCryptography {
    /**
     * encrypting so that the file can not be read
     *
     * @param plainText
     * @return
     */
    public abstract String encrypt(String plainText);

    /**
     * decrypting the file to make it readable
     *
     * @param cipherText
     * @return
     */
    public abstract String decrypt(String cipherText);

}
