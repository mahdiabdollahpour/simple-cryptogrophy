import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * contains corresponding integer for chars and vice versa
 */
public class Coding {
    /**
     * maps the integers to the corresponding character
     */
    public static HashMap<Integer, Character> charTable = new HashMap<Integer, Character>();
    /**
     * maps the characters to the corresponding integer
     */
    public static HashMap<Character, Integer> intTable = new HashMap<Character, Integer>();

    /**
     * intializes the Hashmaps
     */
    static {
        for (int i = 0; i < 26; i++) {

            charTable.put(i, (char) (i + 65));
        }
        for (int i = 26; i < 26 + 26; i++) {

            charTable.put(i, (char) (i + 97 - 26));
        }
        for (int i = 52; i < 52 + 10; i++) {

            charTable.put(i, (char) (i + 48 - 52));
        }
        charTable.put(62, '+');
        charTable.put(63, '/');


        for (int i = 0; i < 26; i++) {

            intTable.put((char) (i + 65), i);
        }
        for (int i = 26; i < 26 + 26; i++) {

            intTable.put((char) (i + 97 - 26), i);
        }
        for (int i = 52; i < 52 + 10; i++) {

            intTable.put((char) (i + 48 - 52), i);
        }
        intTable.put('+', 62);
        intTable.put('/', 63);

    }


    /**
     * holds the last bits cause some (2 or 4) bits remain when you seprate bits 6 by 6
     */
    public ArrayList<Short> lastbits;

    {
        lastbits = new ArrayList<Short>();
    }


    /**
     * calls toChar an to Bit na dconstructs an InputFileReader object and calls parse method
     *
     * @param file the file that is being encoded (sends it to parse)
     * @param skip the bytes that you skip (sends it to parse)
     * @param len  number of bytes tou want to read (sends it to parse)
     * @throws IOException
     */
    public String Encode(File file, int skip, int len) throws IOException {
        return toChar(toBit(new InputFileReader().parse(file, skip, len)));
    }


    /**
     * splits byte array to bits
     *
     * @param byteArray
     */
    private ArrayList<Short> toBit(ArrayList<Short> byteArray) {
        ArrayList<Short> bitArray = new ArrayList<Short>();
        bitArray.clear();
        for (int i = 0; i < byteArray.size(); i++) {
            int a = (int) byteArray.get(i);
            for (int j = 7; j >= 0; j--) {
                bitArray.add((short) (a / (int) Math.pow(2, j)));
                a = a % (int) Math.pow(2, j);
            }
        }
        return bitArray;
    }

    /**
     * seprates bits 6 by 6 and returns a string of the chars corresponding to each 6 bit
     *
     * @return
     */
    private String toChar(ArrayList<Short> bitArray) {
        StringBuilder encodedText = new StringBuilder();
        int remain = bitArray.size() % 6;
        for (int i = 0; i < bitArray.size() - remain; i = i + 6) {
            int sum = 0;
            for (int j = i; j < i + 6 && j < bitArray.size() - remain; j++) {

                sum = sum * 2 + bitArray.get(j);
            }

            encodedText.append(Coding.charTable.get(sum));
        }
        for (int j = bitArray.size() - remain; j < bitArray.size(); j++) {
            lastbits.add(bitArray.get(j));
        }
        return encodedText.toString();
    }


    /**
     * constructs an OutPutFileWriter object and calls writeToFile onject and also calls toByte and ToBit method
     * from this class
     * @param outputFileDir
     * @param textToDecode

     * @throws IOException
     */

    public void Decode(String outputFileDir, String textToDecode) throws IOException {
        new OutPutFileWriter().writeToFile(outputFileDir, toByte(toBit(textToDecode)));
    }

    /**
     * takes the text and returns and arraylist of Short values corresponding to bits
     *
     * @param textToDecode
     * @return
     */

    private ArrayList<Short> toBit(String textToDecode) {
        ArrayList<Short> bitArray = new ArrayList<Short>();
        for (int i = 0; i < textToDecode.length(); i++) {

            int a = intTable.get(textToDecode.charAt(i));

            for (int j = 5; j >= 0; j--) {
                bitArray.add((short) (a / (int) Math.pow(2, j)));
                a = a % (int) Math.pow(2, j);
            }

        }
        if (lastbits.isEmpty() == false) {
            for (int j = 0; j < lastbits.size(); j++) {
                bitArray.add(lastbits.get(j));
            }
        }
        return bitArray;
    }

    /**
     * seprates bits 8 by 8 and stores bytes into byteArray
     *
     * @param bitArray
     */
    private ArrayList<Short> toByte(ArrayList<Short> bitArray) {
        ArrayList<Short> byteArray = new ArrayList<Short>();
        byteArray.clear();
        for (int i = 0; i < bitArray.size(); i += 8) {
            short sum = 0;
            for (int j = i; j < i + 8 && j < bitArray.size(); j++) {
                sum = (short) (2 * sum + bitArray.get(j));
            }
            byteArray.add(sum);
        }
        return byteArray;
    }


}
