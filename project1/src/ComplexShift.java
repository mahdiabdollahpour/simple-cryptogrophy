import java.util.ArrayList;

/**
 * complexShift shifts the chars according to a key that is a sequence of characters
 */
public class ComplexShift extends BaseCryptography {
    /**
     * hols the key as an ArrayList of Characters
     */
    private ArrayList<Character> key;

    {
        key = new ArrayList<Character>();
    }

    /**
     * constructs the object and sets value to key
     *
     * @param key
     */
    public ComplexShift(ArrayList<Character> key) {
        this.key.addAll(key);
    }

    /**
     * encrypting so that the file can not be read
     *
     * @param plainText
     * @return
     */
    public String encrypt(String plainText) {
        StringBuilder enc = new StringBuilder();
        for (int i = 0; i < plainText.length(); i++) {
            int index = Coding.intTable.get(plainText.charAt(i)) +
                    Coding.intTable.get(key.get(i % key.size()));
            while (index < 0) {
                index += 64;
            }
            char c = Coding.charTable.get((index) % 64);
            enc.append(c);
        }
        return enc.toString();
    }

    /**
     * decrypting the file to make it readable
     *
     * @param cipherText
     * @return
     */
    public String decrypt(String cipherText) {
        StringBuilder dec = new StringBuilder();
        for (int i = 0; i < cipherText.length(); i++) {
            int index = (Coding.intTable.get(cipherText.charAt(i)) -
                    Coding.intTable.get(key.get(i % key.size())));
            while (index < 0) {
                index += 64;
            }
            char c = Coding.charTable.get((index) % 64);
            dec.append(c);
        }
        return dec.toString();
    }

}
