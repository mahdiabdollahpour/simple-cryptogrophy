import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class InputFileReader {

    /**
     * reading the file using File input stream
     *
     * @param file the file that is being encoded
     * @param skip the bytes that you skip
     * @param len  number of bytes tou want to read
     * @return ArrayList holding value of bytes
     * @throws IOException becuase of using file input stream
     */
    public ArrayList<Short> parse(File file, int skip, int len) throws IOException {
        FileInputStream in = null;
        ArrayList<Short> byteArray = new ArrayList<Short>();
        byte[] byt;
        in = new FileInputStream(file.getAbsolutePath());
        try {

            int length = Math.min(len, (int) file.length() - skip);
            byt = new byte[length];
            in.skip(skip);
            in.read(byt);
            for (int i = 0; i < byt.length; i++) {
                byte b = byt[i];
                short a = (short) (b & 0xFF);
                byteArray.add(a);
            }
        } finally {

            in.close();
        }
        return byteArray;
    }

}
