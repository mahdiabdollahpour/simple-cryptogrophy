import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;


public class Main {
    private static File[] inputFiles = null;
    private static String outputDirectory = null;
    private static String inputDir = null;
    private static String outputPath = null;
    private static Boolean remove = false;
    private static Boolean simpleshift = false;
    private static Boolean complexshift = false;
    private static Boolean encrypt = false;
    private static Boolean decrypt = false;
    private static String stringKey = null;
    private static int integerKey;
    private static ArrayList<Character> keyArray;


    /**
     * checks the input and warns if something is missing
     *
     * @return
     */
    private static Boolean checkValidation() {
        if (encrypt == false && decrypt == false) {
            System.out.println("Switch not valid or NO switch");
            return false;
        }
        if (simpleshift == false && complexshift == false) {
            System.out.println("Switch not valid or NO switch");
            return false;
        }
        if (inputDir == null) {
            System.out.println("Invalid or NO input Direction");
            return false;
        }


        if (new File(inputDir).exists() == false) {
            System.out.println("Invalid input Direction - It does not exist!");
            return false;
        }

        return true;
    }

    /**
     * add .pbe when ecrypting and removes it when decrypting and set output directory if it has not
     * been defined by user
     */
    private static void handleNaming() {
        if (outputPath == null) {
            outputPath = inputDir;
            if (decrypt) {
                if (inputDir.endsWith(".pbe")) {
                    outputPath = inputDir.substring(0, inputDir.length() - 4);
                } else {
                    String extension = inputDir.substring(inputDir.lastIndexOf('.'), inputDir.length() - 1);
                    outputPath = inputDir.substring(0, inputDir.lastIndexOf('.') - 1);
                    outputPath = outputPath.concat("-decrypted").concat(extension);
                }
            }
            if (encrypt) {
                outputPath = inputDir.concat(".pbe");
            }
        } else if (new

                File(inputDir).

                exists() && !new

                File(inputDir).

                isDirectory()
                && new

                File(outputPath).

                isDirectory())

        {
            outputPath = outputPath.concat("\\").concat(new File(inputDir).getName());

            if (encrypt) {
                outputPath = outputPath.concat(".pbe");
            }
            if (decrypt) {
                if (outputPath.endsWith(".pbe")) {
                    outputPath = outputPath.substring(0, outputPath.length() - 4);
                }
            }
        }


    }
    

    /**
     * does the opration according to tha input
     */
    private static void peform() {
        if (checkValidation() == false) {
            return;
        }
        String temp = outputPath;

        File fi = new File(temp);
        if (fi.exists()) {
            fi.delete();
        }
        try {

            if (simpleshift && encrypt) {
                int start = 0;
                File f = new File(inputDir);
                long end = f.length();
                while (start < end) {

                    Coding c = new Coding();
                    SimpleShift ss = new SimpleShift(integerKey);
                    c.Decode(outputPath, ss.encrypt(c.Encode(f, start, 3000)));
                    start += 3000;

                }

            } else if (simpleshift && decrypt) {

                int start = 0;
                File f = new File(inputDir);
                long end = f.length();
                while (start < end) {

                    Coding c = new Coding();
                    SimpleShift ss = new SimpleShift(integerKey);
                    c.Decode(outputPath, ss.decrypt(c.Encode(new File(inputDir), start, 3000)));
                    start += 3000;

                }

            } else if (complexshift && encrypt) {
                int start = 0;
                File f = new File(inputDir);
                long end = f.length();
                while (start < end) {
                    Coding c = new Coding();
                    ComplexShift cs = new ComplexShift(keyArray);
                    c.Decode(outputPath, cs.encrypt(c.Encode(new File(inputDir), start, 3000)));
                    start += 3000;
                }

            } else if (complexshift && decrypt) {
                int start = 0;
                File f = new File(inputDir);
                long end = f.length();
                while (start < end) {
                    Coding c = new Coding();
                    ComplexShift cs = new ComplexShift(keyArray);
                    c.Decode(outputPath, cs.decrypt(c.Encode(new File(inputDir), start, 3000)));
                    start += 3000;
                }
            }
            if (remove) {
                File f = new File(inputDir);
                f.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * parses args array and finds out the what is the desired operation
     *
     * @param args
     */
    private static void parseArgs(String[] args) {
        Scanner in = new Scanner(System.in);

        try {
            for (int i = 0; i < args.length; i++) {
                args[i] = args[i].toLowerCase();
                if (args[i].equals("-es")) {

                    simpleshift = true;
                    encrypt = true;
                    integerKey = Integer.parseInt(args[i + 1]);
                    i++;
                    continue;
                } else if (args[i].equals("-ec")) {

                    complexshift = true;
                    encrypt = true;
                    stringKey = args[i + 1];
                    i++;
                    continue;
                } else if (args[i].equals("-ds")) {

                    simpleshift = true;
                    decrypt = true;
                    integerKey = Integer.parseInt(args[i + 1]);
                    i++;
                    continue;
                } else if (args[i].equals("-dc")) {

                    complexshift = true;
                    decrypt = true;
                    stringKey = args[i + 1];
                    i++;
                    continue;
                } else if (args[i].equals("-i")) {
                    inputDir = args[i + 1];
                    i++;
                    continue;
                } else if (args[i].equals("-o")) {
                    outputPath = args[i + 1];
                    i++;
                    continue;
                } else if (args[i].equals("-r")) {
                    remove = true;
                }
            }
            keyArray = new ArrayList<Character>();
            if (stringKey != null) {
                for (int i = 0; i < stringKey.length(); i++) {
                    keyArray.add(stringKey.charAt(i));
                }
            }

        } finally {


        }


    }

    public static void main(String[] args) {


        parseArgs(args);
        if (!checkValidation()) {
            return;
        }
        System.out.println(inputDir);
        System.out.println(outputPath);
        if (outputPath != null) {
            new File(outputPath).mkdir();


            if (new File(outputPath).isDirectory()) {
                outputDirectory = outputPath;
            }
        }
        if (new File(inputDir).isDirectory()) {
            inputFiles = new File(inputDir).listFiles();
            for (File f : inputFiles) {
                if (!f.isDirectory()) {
                    inputDir = f.getAbsolutePath();

                    if (outputDirectory != null) {
                        outputPath = outputDirectory.concat("\\").concat(new File(inputDir).getName());
                        if (encrypt) {
                            outputPath = outputPath.concat(".pbe");
                        }
                        if (decrypt) {
                            if (outputPath.endsWith(".pbe")) {
                                outputPath = outputPath.substring(0, outputPath.length() - 4);
                            }
                        }

                    } else {
                        outputPath = null;
                    }
                    handleNaming();

                    peform();
                }
            }
        } else {
            handleNaming();
            peform();
        }

    }


}

