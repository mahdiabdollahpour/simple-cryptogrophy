import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class OutPutFileWriter {

    /**
     * writes bytes to file using FileOutputStream
     * @param outputFile
     * @param byteArray
     * @throws IOException
     */

    public void writeToFile(String outputFile, ArrayList<Short> byteArray) throws IOException {

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(outputFile, true);
            for (int i = 0; i < byteArray.size(); i++) {
                fos.write((int) byteArray.get(i));
            }
        } finally {
            fos.close();
        }
    }
}
