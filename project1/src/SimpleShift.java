/**
 * SimpleShift shifts the chars according to a key that is an integer value
 */
public class SimpleShift extends BaseCryptography {
    /**
     * key value denoting the length of shift
     */
    private int key;

    /**
     * constructs the object and sets value to key
     *
     * @param key
     */
    public SimpleShift(int key) {
        this.key = key;
    }

    /**
     * encrypting so that the file can not be read according to integer key
     *
     * @param plainText
     * @return
     */
    public String encrypt(String plainText) {
        StringBuilder enc = new StringBuilder();
        for (int i = 0; i < plainText.length(); i++) {
            int index = Coding.intTable.get(plainText.charAt(i)) + key;
            while (index < 0) {
                index += 64;
            }
            char c = Coding.charTable.get(index % 64);
            enc.append(c);
        }
        return enc.toString();
    }

    /**
     * decrypting the file to make it readable according to integer key
     *
     * @param cipherText
     * @return
     */
    public String decrypt(String cipherText) {
        StringBuilder dec = new StringBuilder();
        for (int i = 0; i < cipherText.length(); i++) {
            int index = Coding.intTable.get(cipherText.charAt(i)) - key;
            while (index < 0) {
                index += 64;
            }
            char c = Coding.charTable.get(
                    (index) % 64);
            dec.append(c);
        }
        return dec.toString();
    }
}
